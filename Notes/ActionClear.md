##Clear Action Types

Clearing is an action that is only available to the police force. Whenever the police is clearing blockades, you will notice a light blue circle around the police agent. There are two ways for the police to clear blockades.

####Old Method
The "oldMethod" (as defined in ActionClear.class) is when the police tries to clear a single Blockade entity. It will usually take the Police a bit of time to clear an entire blockade depending on the "repair cost" of the blockade. The syntax to create an ActionClear object with the old method is `new ActionClear(EntityID blockadeID)` or `new ActionClear(Blockade blockade)`
    ![Old Method](images/ClearOldMethod.png)

####New Method
The "newMethod" (as it is not the oldMethod) is when the police clears a rect in front of it. The length of the rect is determined by the ScenarioInfo object's `getClearRepairDistance()` method. This method can clear sections of multiple blockades. It is also less likely to require multiple clears before the rect is cleared as opposed to the old method. This is most likely because repairing a rect is less cost intensive than clearing an entire blockade. If one of the blockades in the rect still has a large enough "repair cost" it can take multiple iterations before the blockade in the rect is cleared. The syntax to create an ActionClear object with the new method is `new ActionClear(int x, int y)` or `new ActionClear(int x, int y, Blockade blockade)`
    ![New Method](images/ClearNewMethod.png)

##Bugs/Weird Stuff Encountered
>When using the New Method of clearing, you may want the agent believe that the clear distance is less than it is. The agent can get indefinitely stuck trying to clear something that is right off the very edge of the rect. I had the clear distance be treated as two less for ClearPathExtAct which fixed this.