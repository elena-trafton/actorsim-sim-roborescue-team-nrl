    exploredBuildings = empty set
    buildingsWithBuriedPeople = empty set
    injuredCiviliansNotInRefuge = empty set
    buildingsOnFire = empty set
    civilianOnBoard = null
    position = null
    
    while isAlive:
    
    	position = id of the area that the ambulance is in	
    	updateInformationFromCommunications(exploredBuildings, buildingsWithBuriedPeople, injuredCiviliansNotInRefuge, buildingsOnFire)
    	sendInformationUpdates
    	action = null
    	
    	if commanded:
    	
    		action = commandedAction if able
    		sendMessage(report on progress)
    		
    	if action = null:
    	 	
	    	if civilianOnBoard != null:
	    	
	    		if inRefuge:
	    		
	    			civilianOnBoard = null
	    			action = unloadAction
	    			
	    		else:
	    		
	    			path = shortest path from position to nearest refuge
	    			action = moveAction(path)
	    	
	    	else if inNonRefugeBuilding:
	    	
	    		if buriedPeopleInBuilding:
	    		
	    			person = getBuriedPerson(position)
	    			action = rescueAction(person)
	    	
	    		else if injuredCivilianInBuilding:
	    		
	    			person = getInjuredCivilian(position)
	    			injuredCiviliansNotInRefuge.remove(person)
	    			civilianOnBoard = person
	    			action = loadAction(person)
	    		
	    	else:
	    	
	    		if buildingsWithBuriedPeople is not empty:
	    		
	    			path = shortest path to closest building in buildingsWithBuriedPeople
	    			
	    			if path is blocked:
	    			
	    				commandPolice(to clear path)
	    				
	    			action = moveAction(path)
	    			
	    		else if injuredCiviliansNotInRefuge is not empty:
	    		
	    			path = shortest path to closest building that contains a civilian from injuredCiviliansNotInRefuge
	    			action = moveAction(path)
	    			
	    		else if there are unexplored buildings not on fire:
	    		
	    			path = shortest path to unexplored building not on fire
	    			
	    			if path is blocked:
	    			
	    				commandPolice(to clear path)
	    				
	    			action = moveAction(path)
	    			
		    	else: 
		    	
		    		action = restAction	
	    		
	    sendMessage(what ambulance is going to do)
	    do action
    