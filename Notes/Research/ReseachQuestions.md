###Independent Variables: 
* City Map used
    * Map size
* Blockades/Rubble
    * How much blockades the scenario can produce
    * Number of times additional blockades can be added to the scenario (aftershocks)
* Fire
    * Potency of fire
    * Starting fires
* Agents (including dispatches)
    * Numbers of agents 
    * Location of agents
    * Clear distance of Police
    * Maximum water capacity of Fire Brigade
    * Range of the Fire Brigade
    * If dispatchers are included
* Civilians 
    * Number of civilians
    * Location of civilians
    * Max health of civilians
* Buildings
    * Starting damage of buildings
    * Number of special buildings (Refuges, Hydrants, and Gas stations)
    * Location of special buildings
* Level of communication between agents (Full, Unreliable, Mixed, Many, Medium, None, Very-Limited)
* Variance of Goal Reasoning/Not using Goal Reasoning
* Algorithms/Contestant code being used
* When the simulation stops
    * From time
    * When civilians are all either safe (in a refuge) or dead
	
###Dependent Variables:
* Score
* Civilians
    * Amount of civilians in refuges by the end of the simulation
    * Amount of civilians that survive/die by the end of the simulation
    * Total HP/damage of all the civilians by the end of the simulation
* Agents
    * Amount of agents that survive/die by the end of the simulation
    * Total HP/damage of all the civilians by the end of the simulation
* Buildings
    * Total damage done to buildings by the end of the simulation
    * Total amount of buildings on fire by the end of the simulation
* Time it takes for all the civilians to be either dead or safe (in a refuge)
* Computational Costs
    * Computation time
    * Computation memory

###Research Questions
* Does applying goal reasoning to any of the previous contestant code affect their average score/performance?
    * Expectation: I think it will or else we screwed up
* Is the addition of goal reasoning more effective with higher levels of communication?
    * Expectation: I think it will as planning should be more effective with communication
* Is the addition of goal reasoning more effective in scenarios with more blockades/fires/damage?
    * Expectation: I believe goal reasoning should have the same effectiveness no matter the damage.
* Is the addition of goal reasoning more effective with more agents?
    * Expectation: I believe it will be since it should help with coordination and initial plans
* Is the addition of goal reasoning more effective with the existence of dispatchers?
    * Expectation: I believe it will be since it should make it easier with a central agent dividing up the work

Line 221 StartKernal.java #important
	
	