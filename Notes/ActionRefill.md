##Refilling
Causes a fire brigade member to begin refilling on water. Also see [ActionRest](ActionRest.md). The syntax to create a new refilling action is `new ActionRefill()`.