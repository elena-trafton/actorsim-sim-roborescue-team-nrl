##Moving
Moving is a an action available to all Human agents (Police, Ambulance, Fire Brigade, and Civilians). 
    ![ActionMove](images/Moving.png)
    
   The syntax for calling ActionMove is `new ActionMove(List<EntityID> path)` or `new ActionMove(List<EntityID> path, int destinationX, int destinationY)`. The path cannot be null. The path should be made up of only EntityIDs of Areas but I have not tested to see what happens otherwise. The agent will move as far along the given path as it can in one time step using a built in pathfinding algorithm that I have not explored. I have yet to also figure out where the limit is set to how far an agent can move as it has appeared inconsistent even across agents of the same type.