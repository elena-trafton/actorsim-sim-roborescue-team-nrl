##Rescuing
Rescuing is an action only available to ambulance teams. It is used to unbury humans (including other agents) in buildings.
    ![Two ambulances rescuing one civilian](images/Rescuing.png)

   Rescuing lowers the targets buriedness by 1 every timestep. More than one ambulance can rescue the same target, causing that target's buriedness to go down by 1 for each ambulance rescuing it. Rescuing is a very slow process so it helps having multiple ambulances doing it. It is important to note that the target will most likely be taking damage from being buried and may die during the process. Ambulances may also take damage if the building is on fire. The syntax to make an ActionRescue is `new ActionRescue(Human human)` or `new ActionRescue(EntityID targetID)`.