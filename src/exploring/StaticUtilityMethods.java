package exploring;

import java.util.Arrays;
import java.util.List;

import rescuecore2.standard.entities.Building;
import rescuecore2.standard.entities.StandardEntityConstants;

public class StaticUtilityMethods {
	public static boolean isOnFireOrWaterDameged(Building building){
        final List<StandardEntityConstants.Fieryness> ignoreFieryness
                = Arrays.asList(StandardEntityConstants.Fieryness.UNBURNT, StandardEntityConstants.Fieryness.BURNT_OUT);

        if (building.isFierynessDefined() && ignoreFieryness.contains(building.getFierynessEnum()) ){
            return false;
        }

        return true;
    }
}
