package exploring;

import rescuecore2.misc.collections.LazyMap;
import rescuecore2.standard.entities.Area;
import rescuecore2.standard.entities.Road;
import rescuecore2.standard.entities.StandardWorldModel;
import rescuecore2.worldmodel.Entity;
import rescuecore2.worldmodel.EntityID;

import java.util.*;

public class Graph {
	private Map<EntityID, Set<EntityID>> graph;
	private List<List<EntityID>> cycles;
	private int accessCount;

	public Graph(StandardWorldModel world) {
		resetAccessCount();
		graph = new LazyMap<EntityID, Set<EntityID>>() {
			@Override
			public Set<EntityID> createValue() {
				return new HashSet<EntityID>();
			}
		};
		for (Entity next : world) {
			if (next instanceof Area) {
				Collection<EntityID> areaNeighbours = ((Area) next).getNeighbours();
				graph.get(next.getID()).addAll(areaNeighbours);
//				for(EntityID id : areaNeighbours) {
//					if(world.getEntity(id) instanceof Road) {
//						graph.get(next.getID()).add(id);
//					}
//				}

			}
		}
		cycles = new Vector<List<EntityID>>();
		//findCycles();
	}

	public Graph(List<Area> areas) {
		resetAccessCount();
		graph = new LazyMap<EntityID, Set<EntityID>>() {
			@Override
			public Set<EntityID> createValue() {
				return new HashSet<EntityID>();
			}
		};
		for (Entity next : areas) {
			if (next instanceof Area) {
				Collection<EntityID> areaNeighbours = ((Area) next).getNeighbours();
				graph.get(next.getID()).addAll(areaNeighbours);
			}
		}
	}

	/**
	 * Retrieves all neighbors of a specific node in the graph.
	 *
	 * @param id the entity id of the node.
	 * @return set containing the entity ids of all neighbor nodes.
	 */
	public Set<EntityID> getNeighbors(EntityID id) {
		accessCount++;
		return graph.get(id);
	}

	public int getAccessCount() {
		return accessCount;
	}

	public void resetAccessCount() {
		this.accessCount = 0;
	}
	
	public List<List<EntityID>> getCycles(){
		return cycles;
	}
	
	private void findCycles() {
		EntityID start = graph.keySet().iterator().next();
		Stack<EntityID> stack = new Stack<EntityID>();
		stack.add(start);
		for(EntityID neighbor : getNeighbors(start)) {
			
			findAllCyclesDFS(neighbor, start, stack);
		}
	}
	
	private synchronized void findAllCyclesDFS(EntityID vert, EntityID parent, Stack<EntityID> stack) {
		stack.push(vert);
		for(EntityID neighbor : getNeighbors(vert)) {
			if(!neighbor.equals(parent)) {
				System.out.println("check");
				int index = stack.indexOf(neighbor);
				if(index >= 0) {
					List<EntityID> cycle = stack.subList(index, stack.size());
					cycles.add(cycle);
				}
				else {
					findAllCyclesDFS(neighbor, vert, stack);
				}
			}
		}
		stack.pop();
		
		
		
	}

	/**
	 * Calculates a cycle within the graph if start is in a cycle. 
	 * Otherwise, it returns a list only containing the start node.
	 * @param start The starting node of the cycle
	 * @return A list containing each node within the cycle. If there was no cycle the list will only contain start
	 */
	public List<EntityID> findCycle(EntityID start) {
		List<EntityID> path = new ArrayList<EntityID>();
		path.add(start);
		for(EntityID neighbor : getNeighbors(start)) {
			if(DFSForCycle(start, neighbor, start, path))
				return path;
		}
		return path;
	}

	private boolean DFSForCycle(EntityID start, EntityID vert, EntityID parent, List<EntityID> path) {
		path.add(vert);
		for(EntityID neighbor : getNeighbors(vert)) {
			if(neighbor.equals(start)
					&& !parent.equals(start)) {
				path.add(neighbor);
				return true;
			}
			if(!path.contains(neighbor)) {
				if(DFSForCycle(start, neighbor, vert, path))
					return true;
			}
		}
		path.remove(path.size() - 1);
		return false;
	}


}