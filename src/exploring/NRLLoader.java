package exploring;

import adf.component.AbstractLoader;
import adf.component.tactics.TacticsAmbulanceCentre;
import adf.component.tactics.TacticsAmbulanceTeam;
import adf.component.tactics.TacticsFireBrigade;
import adf.component.tactics.TacticsFireStation;
import adf.component.tactics.TacticsPoliceForce;
import adf.component.tactics.TacticsPoliceOffice;
import adf.sample.tactics.SampleTacticsAmbulanceCentre;
import adf.sample.tactics.SampleTacticsFireStation;
import adf.sample.tactics.SampleTacticsPoliceOffice;

public class NRLLoader extends AbstractLoader {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(NRLLoader.class);

	@Override
	public String getTeamName() {
		return "NRL";
	}

	@Override
    public TacticsAmbulanceTeam getTacticsAmbulanceTeam() {
        TacticsAmbulanceTeam tactics = new CounterClockWiseAmbulanceTactics();
        logger.info("Loaded tactics {}", tactics.getClass());
        return tactics;
    }

    @Override
    public TacticsFireBrigade getTacticsFireBrigade() {
        return new CounterClockWiseFireTactics();
    }

    @Override
    public TacticsPoliceForce getTacticsPoliceForce() {
        return new CounterClockWisePoliceTactics();
    }

    @Override
    public TacticsAmbulanceCentre getTacticsAmbulanceCentre() {
        return new SampleTacticsAmbulanceCentre();
    }

    @Override
    public TacticsFireStation getTacticsFireStation() {
        return new SampleTacticsFireStation();
    }

    @Override
    public TacticsPoliceOffice getTacticsPoliceOffice() {
        return new SampleTacticsPoliceOffice();
    }
}
