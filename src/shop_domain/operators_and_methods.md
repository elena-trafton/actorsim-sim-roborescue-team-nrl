Describes the former pyhop methods, reordered top-down and slightly modified for shop.
A similar file of the same name exists in ../pyhop_domain that reflects the original methods.

This will be used as part of designing the SHOP domain for Roborescue.


Methods available to the ambulance team
---------------------------------------
ambulance_act(agent):
    if found_buried:
        target := select_best_buried_target(agent)
	!move_to(agent, target.location)
	!unbury(agent, target)
	shelter := select_best_shelter(agent)
	transport(agent, target, shelter)
    elif found_injured:
        target := select_best_injured_target(agent)
	!move_to(agent, target.location)
	shelter := select_best_shelter(agent)
	transport(agent, target, shelter)
    else:
        explore(agent)

transport(agent, target, shelter):
    load(agent, target)
    move_to(agent, shelter)
    unload(agent, target)

        
Methods available to the fire brigade
-------------------------------------
fire_act(agent):
    if out_of_water:
       refill_target = select_best_refill_location(agent)
       move_near(agent, refill_target)
       !refill(agent, refill_target)
    elif burning_buildings:
       target = get_closest_burning_building(agent)
       move_near(agent, target)
       extinguish(agent, target)
    else:
       explore(agent)


Methods available for the police force
--------------------------------------
police_act(agent):
    if stuck_agents:
        target := select_best_stuck_agent(agent)
    	go_to_entity_and_clear(agent, target)
    elif stuck_civilians:
        target := select_best_stuck_civilian(agent)
    	go_to_entity_and_clear(agent, target)
    elif unclear_roads_exist:
        target := select_best_unclear_road(agent)
    	go_to_entity_and_clear(agent, target)
    elif blockades_exist:
        target := select_best_blockade(agent)
        move_near(agent, target)
        clear_current_area(agent)
    else:
        explore(agent)

go_to_entity_and_clear(agent, entity):
    clear_road_to(agent, entity.location)
    clear_current_area(agent)


Methods available to all agents
-------------------------------
explore(agent):
    target := select_best_building_or_road(agent)
    move_to(agent, target)
    

Operators available to all agents
---------------------------------
!move(agent, region):

!move_to_point(agent, region):

!move_to_point(agent, region, point_within_region):

!move_near(agent, region, minimum_distance):

!rest(agent):


Operators available to the ambulance team 
-----------------------------------------
!unbury(agent, target):
  - unburies the target; must be in same location as target
!load(agent, target):
  - loads the target; must in same location as target
!unload(agent):
  - unloads the target; target will be at current location


Operators available for the police force
----------------------------------------
!clear(agent, clear_x, clear_y, target_blockade):

!clear_road_to(agent, target):

!clear_current_area(agent):


Operators available for the fire brigade
----------------------------------------
!extinguish(agent, target, water_power):

!refill(agent):
