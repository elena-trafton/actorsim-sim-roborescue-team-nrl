;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; file is ordered top-down so that more abstract methods are at the top
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;
;; Predicates used in this file include:
;;
;; types:
;;   (type_xxx ?item) for the type of an item, see malmo_types.py
;;   (type_location ?location) for location bindings
;;
;; axioms:
;;   TBD
;;
;; entities:
;;   (entity_at ?entity-name ?location)
;;  
;; placeholders:
;;   (placeholder placeholder_name) for placeholders
;;
;;
