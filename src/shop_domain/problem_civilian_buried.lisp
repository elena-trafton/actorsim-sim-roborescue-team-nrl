(defproblem problem roborescue 
  (
   (type_ambulance ambulance_1)
   (available ambulance_1)

   (type_civilian civilian_1)
   (buried civilian_1)

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;; PLACEHOLDERS -- at end of file in case binding 
   ;;;                 uses earlier objects first
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

   (placeholder placeholder_shelter)
   (type_shelter placeholder_shelter)
   
   (placeholder placeholder_civilian)
   (type_civilian placeholder_shelter)
   
   )
  ((transport_hurt_civilian civilian_1))
)
