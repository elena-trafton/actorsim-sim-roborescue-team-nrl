(defproblem problem roborescue 
  (
   (type_brigade brigade_1)
   (available brigade_1)

   (type_brigade brigade_2)
   (available brigade_2)

   (type_building building_1)
   (burning building_1)

   (type_building building_2)
   (burning building_2)

   (type_building building_3)
   (burning building_3)

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;; PLACEHOLDERS -- at end of file in case binding 
   ;;;                 uses earlier objects first
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

   (placeholder placeholder_shelter)
   (type_shelter placeholder_shelter)
   
   (placeholder placeholder_civilian)
   (type_civilian placeholder_shelter)
   
   (placeholder placeholder_brigade)
   (type_brigade placeholder_brigade)
   
   )
  ((douse_fires))
)
