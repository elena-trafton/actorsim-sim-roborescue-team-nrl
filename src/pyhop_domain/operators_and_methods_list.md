Describes the former pyhop methods, reordered top-down and slightly modified.

This will be used as part of designing the SHOP domain for Roborescue.


Methods available to the ambulance team
---------------------------------------
ambulance_act(state, agent_id):
    if found_buried:
        target := get_closest_buried_target()
	move_to(target)
	save(target)
    elif found_injured:
        target := get_closest_injured_target()
	move_to(target)
	carry(target)
    else:
        explore()

save_buried_civilian(state, agent_id, target_id):
    rescue(target)
    carry_injured(target)

save_buried_agent(state, agent_id, target_id):
    rescue(target)

carry_injured(state, agent_id, target_id):
    load(target)
    move_to_closest_refuge()
    unload(target)

        
Methods available to the fire brigade
-------------------------------------
fire_act(state, agent_id):
    if out_of_water:
       refill_at_nearest()
    elif burning_buildings:
       target = get_closest_burning_building()
       move_near(target)
       extinguish(target)
    else:
        explore()

refill_at_nearest_hydrant(state, agent_id):
    move_to_closest_hydrant()
    refill()
    
refill_at_nearest_refuge(state, agent_id):
    move_to_closest_refuge()
    refill()


Methods available for the police force
--------------------------------------
police_act(state, agent_id):
    if stuck_agents:
        free_all_agents()
    elif stuck_civilians:
        free_all_civilians()
    elif unclear_roads_exist:
        unclear_paths_to_all_buildings()
    elif blockades_exist:
        clear_closest_blockade()
    else:
        explore()

free_all_agents(state, agent_id):
    free_nearest_agent()
    
free_all_civilians(state, agent_id):
    free_nearest_civilian()


unclear_paths_to_all_buildings(state, agent_id, road_set):
    unclear_nearest_building()

clear_closest_blockade(state, agent_id):
    target := get_closest_blockade()
    move_near(target)
    clear_current_area()
	
free_nearest_agent(state, agent_id):
    target := get_closest_stuck_agent()
    go_to_entity_id_and_clear(target)
    
free_nearest_civilian(state, agent_id):
    target := get_closest_stuck_civilian()
    go_to_entity_id_and_clear(target)
    
unclear_nearest_building(state, agent_id, road_clear_set):
    target := get_closest_unclear_road()
    go_to_entity_id_and_clear(target)

go_to_entity_id_and_clear(state, agent_id, entity_id):
    entity_loc := location(entity_id)
    clear_road(entity_loc)
    clear_current_area()


Methods available to all agents
-------------------------------
explore_buildings(state, agent_id):
    move_to_closest(unexplored_building)

explore_roads(state, agent_id):
    move_to_closest(unexplored_road)

    

Operators available to all agents
---------------------------------
move(state, agent_id, destination_id):

move_to(state, agent_id, destination_id, end_x, end_y):

move_near(state, agent_id, destination_id, range):

rest(state, agent_id):


Operators available to the ambulance team 
-----------------------------------------
rescue(state, agent_id, target_id):

load(state, agent_id, target_id):

unload(state, agent_id):


Operators available for the police force
----------------------------------------
clear(state, agent_id, clear_x, clear_y, target_blockade):

clear_road(state, agent_id, end_position, end_x, end_y):

clear_current_area(state, agent_id):


Operators available for the fire brigade
----------------------------------------
extinguish(state, agent_id, target_id, water_power):

refill(state, agent_id):
