#  Installing RoboRescue from scratch

**THE DIRECTIONS BELOW HERE ARE FOR THE OLD SETUP. 
You probably won't need to use these.**

[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
## Setup IntelliJ IDEA
Setting up IntelliJ involves creating a way to run the server using and External 
Tool and

### Add an external tool to run rr-server in IntelliJ
0. Open IntelliJ
1. Go to: **Run** > **Edit Configurations**  to open *Run/Debug Configurations* window for rr-server.
2. On Add New Application menu, click **Application**.
3. Provide a **Name** for configuration (e.g., NRLBasicTest) 
4. On *Configuration* tab
	- Set Main class as kernel.StartKernel
	- Set Working directory as rr-server/boot
	- Set Use classpath of module rr-server.main
	- On *Before Launch: Build, External tool, Activate tool* , click add
		- Click **Run external tool** > **External Tool** > **Create Tool**
		- On the *Create Tool* window provide a name. E.g., rr-server-basic
		- On Tool Settings:
			- Set Program as	 /bin/bash
			- Set Arguments as	./start_in_intellij.sh -m ../maps/gml/test/map -c ../maps/gml/test/config 
			- Set working directory as rr-server/boot
			- Save configuration
	- Set the order for* Before Launch*: as (1) Build (2) External Tool
	- Click **Run**

When this configuration is run, it will first build the rr-server source.  Then it kills any active servers and runs the External tool command, which will execute the shell script start.sh with the given map/config arguments. If the build is successful, a UI window for the server should appear.

The image below shows the test server tool setup.
![ExternalTool](_images/ExternalToolToRunServer.png)


[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
#### OPTIONAL: Set up rr-server project on IntelliJ

 OPTIONAL: This step is only needed when you want for server development.  If you are in doubt, skip this step.

1. Open IntelliJ 
2. Go to:  **File** > **New** > **Project From Existing Sources**
3. In pop-up window *Select File or Directory to Import*, choose the top-level directory in rr-server that contains **build.gradle** file. 
4. In the next pop-up window *Import Project* , choose option Import project from external model and select **Gradle**. Click Next.
5. In the next step make the following changes:
	- Set the path for Gradle project to where **build.gradle** file in rr-server directory.
	- Choose Use gradle 'wrapper' task configuration option
	- Click Finish
6. The project opens with the source view on *Project* window on IntelliJ

### Open the TeamNRL RoboRescue client in IntelliJ
The server runs a set of one or more agents (e.g., civilians, police, fire brigade, ambulance) along with exogenous processes such as the fire or earthquake simulators.
We talk with the server by running a client, and a single client application can command multiple agents.

You can open the existing TeamNRL project or create a new one.  We suggest trying to open the existing project as a first step. 

#### Using the default project (default)

0. Open IntelliJ
1. Go to: **File** > **Open**
2. Open the project TEAM_NRL_HOME/idea_projects/team_nrl
3. If IntelliJ asks to know the location of ACTORSIM_HOME use the value from above.



[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
#### OPTIONAL: Creating a new project from scratch (expert)

1. Open IntelliJ 
2. Go to:  **File** > **New** > **Project From Existing Sources**
3. In pop-up window *Select File or Directory to Import*, choose the top-level directory that contains **build.gradle** file in ``team-nrl-git``.
4. In the next pop-up window *Import Project* , choose option Import project from external model and select **Gradle**. Click Next.
5. In the next step make the following changes:
	- Set the path for Gradle project to where **build.gradle** file in rr-team-nrl directory.
	- Choose Use gradle 'wrapper' task configuration option
	- Click Finish
6. The project opens with the source view on *Project* window on IntelliJ
7. Go to: ** File**  > **Project Structure** > **Project Settings** > **Libraries**
Add all .jar files in following paths and click Apply to save settings
	- TEAM_NRL_HOME/library/util/default/
	- TEAM_NRL_HOME/library/rescue/adf/
	- TEAM_NRL_HOME/library/rescue/rescuecore/
	- TEAM_NRL_HOME/library/util/
8. Go to: ** File**  > **Project Structure** > **Project Settings** > **Modules**
	- Make sure the content root is pointing to <TEAM_NRL_HOME>/src/
	- Mark the following source directories as excluded (select > right click > click Excluded)
		- pyhop_domain
		- python_code
		- shop_domain
	- Click Apply to save the settings

	
[comment]: ==================================================================
[comment]: ==================================================================
[comment]: ==================================================================
*****************************************************************************
### Add a run configuration for rr-team-nrl
1. Go to: **Run** > **Edit Configurations**  to open *Run/Debug Configurations* window for rr-team-nrl.
2. On Add New Application menu, click **Application**.
3. Provide a **Name** for configuration. E.g., rr-team [run] 
4. On *Configuration* tab:
	- Set Main class as adf.Main
	- Set Working directory as rr-team-nrl/
	- Set Use classpath of module rr-team-nrl
	- Set Program Arguments as nrl.NRLCentralizedDispatcher -all -d 1
	- On *Before Launch: Build, External tool, Activate tool* , click add
		- Click **Run external tool** > **External Tool** > **Create Tool**
		- On the *Create Tool* window provide a name (e.g., RR Test5)
		- On Tool Settings:
			- Set Program as	 /bin/bash
			- Set Arguments as	start_in_intellij_idea.sh ../maps/gml/Test5
			- Set working directory as <SERVER_HOME>/boot
			- Save configuration
	- Set the order for* Before Launch*: as (1) Build (2) External Tool
	- Click **Run**
With this run configuration, the simulator will launch the kernel and load the
rescue scenario having 15 ambulances. You can load a different scenario by
adding another run configuration. When adding a new run configuration, follow
the same process as above step (v) items 1 through 4 and modify the scenario 
in **Arguments** of run configuration. For example to add a new configuration 
for scenario test5 set Arguments in run configuration as: 
`start_in_intellij_idea.sh ../maps/gml/test5`.


The image below shows the setup for running the TeamNRL CentralizedDispatcher 
![test](_images/CentralizedDispatcherOnTest5.png)
	

## Coding tips


You can sometimes cause an agent to stay put by setting a breakpoint in think() that causes it to fail to tell the server a new command. The server will then refuse to update that agent.