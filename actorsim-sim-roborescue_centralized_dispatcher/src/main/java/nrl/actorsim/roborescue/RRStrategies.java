package nrl.actorsim.roborescue;

import nrl.actorsim.chronicle.GoalOrdering;
import nrl.actorsim.cognitive.StrategyGroup;
import nrl.actorsim.goalnetwork.GoalLifecycleNode;
import nrl.actorsim.memory.*;
import nrl.actorsim.goalrefinement.strategies.*;
import nrl.actorsim.roborescue.NRLTacticsUtilities.ClosestResult;
import org.slf4j.Logger;

import java.util.*;
import java.util.List;

import static nrl.actorsim.chronicle.AllenIntervalOperator.BEFORE;
import static nrl.actorsim.goalrefinement.strategies.GoalMode.*;
import static nrl.actorsim.roborescue.RRStrategies.*;
import static nrl.actorsim.roborescue.RRWorldObject.NULL_RR_WORLD_OBJECT;

public class RRStrategies extends StrategyGroup {
    static boolean traceBaseGoals = true;
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(RRStrategies.class);

    RRStrategies(WorkingMemory memory) {
        addApplyOnceAtEndEachCycle(new RRBaseGoalStrategies.UpdateStatusForRRGoals());
        addAll(new AmbulanceStrategies(memory));
        addAll(new FireBrigadeStrategies(memory));
        addAll(new PoliceStrategies(memory));
    }
}

class AmbulanceStrategies extends StrategyGroup {
    AmbulanceStrategies(WorkingMemory memory) {
        GoalMemorySimple goalMemory = memory.getGoalMemory();

        goalMemory.add(new GoalOrdering(RRGoalUnburyAgent.class, BEFORE, RRGoalRescueCivilian.class));
        goalMemory.add(new GoalOrdering(RRGoalRescueCivilian.class, BEFORE, RRGoalScoutBuilding.class));

        List<RRGoalBase> goals = Arrays.asList(
                new RRGoalUnburyAgent(),
                new RRGoalRescueCivilian(),
                new RRGoalScoutBuilding()
        );

        RRBaseGoalStrategies.addBaseGoalStrategies(this, goals);
    }
}

class FireBrigadeStrategies extends StrategyGroup {
    FireBrigadeStrategies(WorkingMemory memory) {
        GoalMemorySimple goalMemory = memory.getGoalMemory();

        goalMemory.add(new GoalOrdering(RRGoalExtinguishBuilding.class, BEFORE, RRGoalScoutBuilding.class));

        List<RRGoalBase> goals = Collections.singletonList(
                new RRGoalExtinguishBuilding()
        );

        RRBaseGoalStrategies.addBaseGoalStrategies(this, goals);
    }
}

class PoliceStrategies extends StrategyGroup {
    PoliceStrategies(WorkingMemory memory) {
        GoalMemorySimple goalMemory = memory.getGoalMemory();

        goalMemory.add(new GoalOrdering(RRGoalFreeAgent.class, BEFORE, RRGoalFreeCivilian.class));
        goalMemory.add(new GoalOrdering(RRGoalFreeCivilian.class, BEFORE, RRGoalClearBlockades.class));
        goalMemory.add(new GoalOrdering(RRGoalClearBlockades.class, BEFORE, RRGoalScoutBuilding.class));

        List<RRGoalBase> goals = Arrays.asList(
                new RRGoalFreeAgent(),
                new RRGoalFreeCivilian(),
                new RRGoalClearBlockades()
        );
        RRBaseGoalStrategies.addBaseGoalStrategies(this, goals);
    }
}

class RRBaseGoalStrategies {
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static void addBaseGoalStrategies(StrategyGroup group, List<RRGoalBase> goals) {
        for (RRGoalBase goal : goals) {
            Class goalClass = goal.getClass();
            group.add(new FormulateRRBaseGoal(goal));
            group.add(new SelectRRBaseGoalByPreemptingAgent(goalClass));
            group.add(new SelectRRBaseGoal(goalClass));
            group.add(new ApplyAlwaysExpandStrategyTemplate(goalClass));
            group.add(new ApplyAlwaysCommitStrategyTemplate(goalClass));
            group.add(new DispatchRRBaseGoal(goalClass));
            group.add(new EvaluateRRBaseGoal(goalClass));
            group.add(new EvaluateRRBaseGoalAssignedToAnotherAgent(goalClass));
            group.add(new FinishRRBaseGoal(goalClass));
            group.add(new ApplyDropToFinishedGoalsStrategyTemplate(goalClass));
        }
    }

    private static class FormulateRRBaseGoal extends ApplyFormulateStrategyTemplate {
        private final RRGoalBase baseGoal;
        public FormulateRRBaseGoal(RRGoalBase baseGoal) {
            super(baseGoal.getClass());
            this.baseGoal = baseGoal;
        }

        @Override
        public void checkPreconditions(WorkingMemory memory, ApplyStrategyDetailsCollection applyCollection) {
            if(baseGoal.isGoalTracingEnabled()) {
                Logger traceLogger = baseGoal.getTraceLogger();
                traceLogger.trace("{}: Checking preconditions", baseGoal);
            }
            if (memory.getGoalMemory().missing(goal -> goal == baseGoal)) {
                if(baseGoal.isGoalTracingEnabled()) {
                    baseGoal.getTraceLogger().trace("{}: missing goal", baseGoal);
                }
                baseGoal.checkApplicablePredicateAndCompletionCondition(memory, applyCollection);
            }
        }

        @Override
        public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
            applyDetails.attemptApply(baseGoal -> {
                if (RRStrategies.traceBaseGoals) {
                    baseGoal.enableGoalTracing();
                }

                if(baseGoal.isGoalTracingEnabled()) {
                    baseGoal.getTraceLogger().trace("Attempting to formulate");
                }

                baseGoal.formulate(memory.getGoalMemory());
            });
        }
    }

    static class SelectRRBaseGoalByPreemptingAgent extends StrategyFilterByGoalClassAndMode {
        Class<? extends RRGoalBase> baseGoalClass;

        public SelectRRBaseGoalByPreemptingAgent(Class<? extends RRGoalBase> baseGoalClass) {
            super(baseGoalClass, FORMULATED);
            this.baseGoalClass = baseGoalClass;
        }

        @Override
        public void checkPreconditions(WorkingMemory memory, ApplyStrategyDetailsCollection applyCollection) {
            insertMatchingGoals(memory, applyCollection, goal -> ((RRGoalBase) goal).hasInterruptableSuccessors(memory));
        }

        @Override
        public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
            applyDetails.attemptApply(subgoal -> ((RRGoalBase)subgoal).hasInterruptableSuccessors(memory), subgoal -> {
                List<GoalLifecycleNode> successors = ((RRGoalBase)subgoal).getSuccessors();
                applyDetails.attemptApplyOnce(successors, successor -> {
                    logger.debug("Attempting to interrupt {}", successor);
                    ((Interuptable) successor).interrupt();
                });
            });
        }
    }

    static class SelectRRBaseGoal extends StrategyFilterByGoalClassAndMode {
        Class<? extends RRGoalBase> subgoalClass;
        public SelectRRBaseGoal(Class<? extends RRGoalBase> subgoalClass) {
            super(subgoalClass, Arrays.asList(FORMULATED, EVALUATED), goal -> (((RRGoalBase) goal).remainsUnassignedByDispatcher())
                    && ((RRGoalBase) goal).getAvailableAgents().size() > 0);
            this.subgoalClass = subgoalClass;
        }

        @Override
        public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
            CentralizedDispatcher dispatcher = CentralizedDispatcher.getInstance();
            GoalLifecycleNode goal = applyDetails.getGoal();
            if (goal instanceof RRGoalBase) {
                RRGoalBase rrGoalBase = (RRGoalBase) goal;
                List<RRWorldObject> availableAgents = rrGoalBase.getAvailableAgents();
                ClosestResult result = dispatcher.getClosestAvailableScoutingAgent(rrGoalBase.getEntity());
                if ((result.closestAgent != NULL_RR_WORLD_OBJECT)
                        && applyDetails.applyNowAndIncrementAttempted()) {
                    rrGoalBase.setSuggestedAssignment(result.closestAgent);
                    if (dispatcher.attemptAssignment(rrGoalBase)) {
                        rrGoalBase.select();
                    } else {
                        rrGoalBase.setSuggestedAssignment(NULL_RR_WORLD_OBJECT);
                    }
                }
            }
        }
    }

    static class DispatchRRBaseGoal extends StrategyFilterByGoalClassAndMode {
        public DispatchRRBaseGoal(Class<? extends RRGoalBase> subgoalClass) {
            super(subgoalClass, Arrays.asList(COMMITTED, EVALUATED), goal ->
                    ((RRGoalBase) goal).isAssignedByDispatcher()
                    || ((RRGoalBase) goal).hasBeenReassigned());
        }

        @Override
        public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
            applyDetails.attemptApply(goal -> {
                RRGoalBase baseGoal = (RRGoalBase) goal;
                if (CentralizedDispatcher.getInstance().executeAssignment(baseGoal)) {
                    baseGoal.dispatch();
                } else {
                    baseGoal.setAssignedByDispatcher(NULL_RR_WORLD_OBJECT);
                }
            });
        }
    }

    static class EvaluateRRBaseGoal extends StrategyFilterByGoalClassAndMode {
        public EvaluateRRBaseGoal(Class<? extends RRGoalBase> baseGoalClass) {
            super(baseGoalClass, DISPATCHED);
        }

        @Override
        public void checkPreconditions(WorkingMemory memory, ApplyStrategyDetailsCollection applyCollection) {
            insertMatchingGoals(memory, applyCollection,
                    goal -> ((RRGoalBase) goal).isComplete(memory));
        }

        @Override
        public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
            applyDetails.attemptApply( baseGoal -> {
                logger.debug("{}: the formerly dispatched goal {} is complete and needs evaluation", getShortName(), baseGoal.getName());
                baseGoal.evaluate();
            });
        }
    }

    static class EvaluateRRBaseGoalAssignedToAnotherAgent extends StrategyFilterByGoalClassAndMode {
        public EvaluateRRBaseGoalAssignedToAnotherAgent(Class<? extends RRGoalBase> baseGoalClass) {
            super(baseGoalClass, DISPATCHED);
        }

        @Override
        public void checkPreconditions(WorkingMemory memory, ApplyStrategyDetailsCollection applyCollection) {
            insertMatchingGoals(memory, applyCollection,
                    goal -> ((RRGoalBase) goal).needsEvaluate());
        }

        @Override
        public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
            applyDetails.attemptApply(subgoal -> {
                logger.debug("{}: the formerly dispatched goal {} needs evaluation", getShortName(), subgoal.getName());
                subgoal.evaluate();
            });
        }
    }

    static class FinishRRBaseGoal extends StrategyFilterByGoalClassAndMode {
        public FinishRRBaseGoal(Class<? extends RRGoalBase> subgoalClass) {
            super(subgoalClass, FORMULATED, SELECTED, EXPANDED, COMMITTED, DISPATCHED, EVALUATED);
        }

        @Override
        public void checkPreconditions(WorkingMemory memory, ApplyStrategyDetailsCollection applyCollection) {
            insertMatchingGoals(memory, applyCollection,
                    goal -> ((RRGoalBase) goal).isComplete(memory));
        }

        @Override
        public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
            applyDetails.attemptApply( goal -> {
                RRGoalBase baseGoal = (RRGoalBase) goal;
                logger.debug("{}: unassigning and finishing goal {}", getShortName(), baseGoal.getName());

                CentralizedDispatcher.getInstance().unassign(baseGoal);
                baseGoal.finish();
            });
        }
    }

    static class UpdateStatusForRRGoals implements ApplyStrategyTemplate {
        @Override
        public boolean logApply() {
            return false;
        }

        @Override
        public String toString() {
            return getShortName();
        }

        @Override
        public boolean matches(GoalLifecycleNode goal) {
            return (goal instanceof RRStatusUpdatable);
        }

        @Override
        public void attemptApply(WorkingMemory memory, ApplyStrategyDetails applyDetails) {
            applyDetails.attemptApply(goal -> ((RRStatusUpdatable) goal).updateStatus());
        }
    }
}
