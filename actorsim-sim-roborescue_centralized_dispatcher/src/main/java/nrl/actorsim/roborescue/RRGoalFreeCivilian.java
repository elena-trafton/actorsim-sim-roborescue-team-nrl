package nrl.actorsim.roborescue;

import adf.agent.communication.standard.bundle.centralized.CommandPolice;
import nrl.actorsim.domain.StateVariable;
import nrl.actorsim.domain.WorldObject;
import nrl.actorsim.domain.WorldType;

import java.util.Collections;

import static nrl.actorsim.roborescue.RRPlanningDomain.BLOCKED_WITH_CIVILIAN;
import static nrl.actorsim.roborescue.RRPlanningDomain.POLICE;

public class RRGoalFreeCivilian extends RRGoalBase {
    RRGoalFreeCivilian() {
        super(CommandPolice.ACTION_CLEAR,
                BLOCKED_WITH_CIVILIAN,
                CompletionCondition.MISSING_FROM_MEMORY,
                Collections.singletonList(POLICE.getUrn()));
    }

    private RRGoalFreeCivilian(RRGoalFreeCivilian template, RRWorldObject road) {
        super(template, road);
    }

    @Override
    public RRGoalBase instance(WorldObject obj) {
        if (obj instanceof RRWorldObject) {
            if (isRoad(obj)) {
                return new RRGoalFreeCivilian(this, (RRWorldObject) obj);
            }
        }
        return NULL_RR_BASE_GOAL;
    }

    @Override
    public boolean matchesVariable(StateVariable sv) {
        return super.matchesVariable(sv)
                && sv.hasBindings()
                && isRoad(sv.getBinding(0));
    }

    private boolean isRoad(WorldType other) {
        return other.equalsOrInheritsFromType(RRPlanningDomain.ROAD);
    }

}
