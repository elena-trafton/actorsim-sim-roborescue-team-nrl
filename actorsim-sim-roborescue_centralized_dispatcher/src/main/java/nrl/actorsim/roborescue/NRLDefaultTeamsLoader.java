package nrl.actorsim.roborescue;

import adf.component.AbstractLoader;
import adf.component.tactics.TacticsAmbulanceCentre;
import adf.component.tactics.TacticsAmbulanceTeam;
import adf.component.tactics.TacticsFireBrigade;
import adf.component.tactics.TacticsFireStation;
import adf.component.tactics.TacticsPoliceForce;
import adf.component.tactics.TacticsPoliceOffice;
import adf.sample.tactics.SampleTacticsAmbulanceCentre;
import adf.sample.tactics.SampleTacticsFireStation;
import adf.sample.tactics.SampleTacticsPoliceOffice;

public class NRLDefaultTeamsLoader extends AbstractLoader{
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(NRLDefaultTeamsLoader.class);

	@Override
	public String getTeamName() {
		return "NRL";
	}

	@Override
    public TacticsAmbulanceTeam getTacticsAmbulanceTeam() { return new NRLAmbulanceTactics(); }

    @Override
    public TacticsFireBrigade getTacticsFireBrigade() {
        return new NRLFireBrigadeTactics();
    }

    @Override
    public TacticsPoliceForce getTacticsPoliceForce() {
        return new NRLPoliceTactics();
    }

    @Override
    public TacticsAmbulanceCentre getTacticsAmbulanceCentre() { return new SampleTacticsAmbulanceCentre(); }

    @Override
    public TacticsFireStation getTacticsFireStation() {
        return new SampleTacticsFireStation();
    }

    @Override
    public TacticsPoliceOffice getTacticsPoliceOffice() {
        return new SampleTacticsPoliceOffice();
    }

}
