package nrl.actorsim.roborescue;

import java.util.*;
import java.util.stream.Collectors;

import adf.agent.action.Action;
import adf.agent.action.common.ActionMove;
import adf.agent.action.common.ActionRest;
import adf.agent.action.police.ActionClear;
import adf.agent.communication.MessageManager;
import adf.agent.communication.standard.bundle.centralized.CommandPolice;
import adf.agent.communication.standard.bundle.centralized.CommandScout;
import adf.agent.communication.standard.bundle.information.MessagePoliceForce;
import adf.agent.develop.DevelopData;
import adf.agent.info.AgentInfo;
import adf.agent.info.ScenarioInfo;
import adf.agent.info.WorldInfo;
import adf.agent.module.ModuleManager;
import adf.agent.precompute.PrecomputeData;
import adf.component.centralized.CommandExecutor;
import adf.component.communication.CommunicationMessage;
import adf.component.module.algorithm.PathPlanning;
import adf.component.tactics.TacticsPoliceForce;
import adf.sample.centralized.CommandExecutorPolice;
import adf.sample.centralized.CommandExecutorScout;
import adf.sample.tactics.utils.MessageTool;
import rescuecore2.misc.Pair;
import rescuecore2.misc.geometry.Point2D;
import rescuecore2.misc.geometry.Vector2D;
import rescuecore2.standard.entities.Area;
import rescuecore2.standard.entities.Blockade;
import rescuecore2.standard.entities.PoliceForce;
import rescuecore2.standard.entities.Road;
import rescuecore2.standard.entities.StandardEntity;
import rescuecore2.standard.entities.StandardEntityURN;
import rescuecore2.worldmodel.EntityID;
import test_team.module.algorithm.AStarPathPlanning;

public class NRLPoliceTactics extends TacticsPoliceForce implements TacticsCommandable {
    final static private org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(NRLPoliceTactics.class);

    private EntityID agentID;
    private String shortName;

    private final static int CLEAR_DISTANCE_LIMITER = 20;
    private final static int REPAIR_COST_WORTH_CLEARING = 1;
    private int clearDistance;

    private PathPlanning pathPlanning;

    private CommandExecutor<CommandPolice> commandExecutorPolice;
    private CommandExecutor<CommandScout> commandExecutorScout;

    private MessageTool messageTool;

    private CommunicationMessage recentCommand;

    private Boolean isVisualDebug;

    NRLAgentStateOptions stateOptions = null;
    NRLAgentState state = null;
    HashSet<EntityID> unexploredBuildings = new HashSet<>();
    HashSet<EntityID> unexploredRoads = new HashSet<>();
    private boolean autonomousActionEnabled = false;

    @Override
    public String getShortName() {
        return this.shortName;
    }

    @Override
    public void initialize(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                           ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {

        worldInfo.indexClass(
                StandardEntityURN.ROAD,
                StandardEntityURN.HYDRANT,
                StandardEntityURN.BUILDING,
                StandardEntityURN.REFUGE,
                StandardEntityURN.BLOCKADE
        );

        this.messageTool = new MessageTool(scenarioInfo, developData);

        this.isVisualDebug = (scenarioInfo.isDebugMode()
                && moduleManager.getModuleConfig().getBooleanValue("VisualDebug", false));
        this.clearDistance = scenarioInfo.getClearRepairDistance() - CLEAR_DISTANCE_LIMITER;
        this.recentCommand = null;

        this.pathPlanning = new AStarPathPlanning(agentInfo, worldInfo, scenarioInfo, moduleManager, developData);
        this.commandExecutorPolice = new CommandExecutorPolice(agentInfo, worldInfo, scenarioInfo, moduleManager, developData);
        this.commandExecutorScout = new CommandExecutorScout(agentInfo, worldInfo, scenarioInfo, moduleManager, developData);
        registerModule(pathPlanning);
        registerModule(commandExecutorPolice);
        registerModule(commandExecutorScout);

        stateOptions = new NRLAgentStateOptions();
        stateOptions.updateUsingCommunications = true;
        stateOptions.parseCivilianMessages = true;
        stateOptions.parsePoliceMessages = true;
        stateOptions.parseRoadsAndBuildings = true;
        state = new NRLAgentState(this, stateOptions);
        state.initialize(worldInfo);

        agentID = agentInfo.getID();
        String agentType = agentInfo.me().getStandardURN().name();
        shortName = agentType + "-" + agentID;
        logger.info("{}: Initilialzed", shortName);
    }

    @Override
    public void precompute(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                           ModuleManager moduleManager, PrecomputeData precomputeData, DevelopData developData) {
        modulesPrecompute(precomputeData);
    }

    @Override
    public void resume(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo, ModuleManager moduleManager,
                       PrecomputeData precomputeData, DevelopData developData) {
        modulesResume(precomputeData);
        NRLTacticsUtilities.showTimestep(isVisualDebug, agentInfo, worldInfo, scenarioInfo);
    }

    @Override
    public void preparate(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                          ModuleManager moduleManager, DevelopData developData) {
        modulesPreparate();
        NRLTacticsUtilities.showTimestep(isVisualDebug, agentInfo, worldInfo, scenarioInfo);
    }

    @Override
    public Action think(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                        ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {
        this.messageTool.reflectMessage(agentInfo, worldInfo, scenarioInfo, messageManager);
        this.messageTool.sendRequestMessages(agentInfo, worldInfo, scenarioInfo, messageManager);
        this.messageTool.sendInformationMessages(agentInfo, worldInfo, scenarioInfo, messageManager);
        modulesUpdateInfo(messageManager);
        updateInfoFromSelf(agentInfo);
        state.update(worldInfo, messageManager);
        NRLTacticsUtilities.showTimestep(isVisualDebug, agentInfo, worldInfo, scenarioInfo);

        PoliceForce agent = (PoliceForce) agentInfo.me();
        Action action = null;

        setRecentCommand(agentInfo, messageManager);
        recentCommand = CentralizedDispatcher.getInstance().getCommandMessage(this.agentID);
        if (this.recentCommand != null) {
            action = followCommand(recentCommand);
        }

        if (autonomousActionEnabled) {
            if (action == null) {
                action = actAutonomously(agentInfo, worldInfo, developData, agent);
            }
        }

        if ((action == null)
            || ((action.getClass() == ActionMove.class)
                && ((ActionMove) action).getPath() == null)) {
            action = new ActionRest();
        }

        EntityID target = getTargetFromAction(worldInfo, action, agentInfo.getPosition());

        logger.debug("{}: sending action {}", shortName, action);
        sendActionMessage(messageManager, agent, action, target);
        return action;
    }

    private void updateInfoFromSelf(AgentInfo agentInfo) {
        if (agentInfo.getPositionArea().getStandardURN() == StandardEntityURN.ROAD) {
            state.exploredRoads.add(agentInfo.getPosition());
        } else if (agentInfo.getPositionArea().getStandardURN() == StandardEntityURN.BUILDING) {
            state.exploredBuildings.add(agentInfo.getPosition());
        }

    }

    private void setRecentCommand(AgentInfo agentInfo, MessageManager messageManager) {
        for (CommunicationMessage message : messageManager.getReceivedMessageList()) {
            if (message instanceof CommandScout) {
                CommandScout command = (CommandScout) message;
                if (command.isToIDDefined() && Objects.requireNonNull(command.getToID()).getValue() == agentInfo.getID().getValue()) {
                    recentCommand = command;
                }
            }
            if (message instanceof CommandPolice) {
                CommandPolice command = (CommandPolice) message;
                if (command.isToIDDefined() && Objects.requireNonNull(command.getToID()).getValue() == agentInfo.getID().getValue()) {
                    recentCommand = command;
                }
            }
        }
    }

    private Action followCommand(CommunicationMessage command) {
        if (command.getClass() == CommandScout.class) {
            commandExecutorScout.setCommand((CommandScout) command);
            return commandExecutorScout.calc().getAction();
        }
        if (command.getClass() == CommandPolice.class) {
            commandExecutorPolice.setCommand((CommandPolice) command);
            return commandExecutorPolice.calc().getAction();
        }
        return null;
    }

    private Action actAutonomously(AgentInfo agentInfo, WorldInfo worldInfo, DevelopData developData, PoliceForce agent) {
        Action action = null;
        EntityID position = agentInfo.getPosition();
        if (positionContainsStuckAgent(worldInfo, position)) {
            action = calculateClearRoadAction(worldInfo, agent, position);
        } else if (!state.stuckAgentsWithPositions.isEmpty()) {
            action = calculateMoveToNearestStuckAgent(worldInfo, agent, position);
        } else if (!state.stuckCiviliansWithPositions.isEmpty()) {
            action = calculateMoveToNearestStuckCivilian(worldInfo, agent, position);

        } else if (hasSurroundingBuildingAndRoadContainsBlockade(worldInfo, position)) {
            action = calculateClearRoadAction(worldInfo, agent, position);
        } else {
            PathWithEndPosition path = this.pathToNearbyBlockedBuilding(worldInfo, developData, position);
            if (path != null) {
                action = calculateClearPath(worldInfo, agent, path);
            } else {
                Collection<EntityID> blockades = worldInfo.getEntityIDsOfType(StandardEntityURN.BLOCKADE);
                if (blockades.size() > 0) {
                    action = calculateClearLeftoverBlockades(worldInfo, agentInfo.getID(), position, blockades);
                } else if (unexploredRoadsRemain()) {
                    action = calculateExploreRoads(position);
                } else if (unexploredBuildingsRemain(worldInfo)) {
                    action = calculateExploreBuildings(worldInfo, position);
                } else {
                    action = new ActionRest();
                }
            }
        }
        return action;
    }

    private boolean positionContainsStuckAgent(WorldInfo worldInfo, EntityID position) {
        for (EntityID stuckAgentID : state.stuckAgentsWithPositions.keySet()) {
            if (state.stuckAgentsWithPositions.get(stuckAgentID).equals(position)) {
                return true;
            }
        }
        return false;
    }


    //This method needs to be refactored
    private Action calculateClearRoadAction(WorldInfo worldInfo, PoliceForce agent, EntityID position) {
        Area area = (Area) worldInfo.getEntity(position);
        if (area instanceof Road) {
            Road road = (Road) area;
            if (road.isBlockadesDefined() && road.getBlockades().size() > 0) {
                Collection<Blockade> blockades = worldInfo.getBlockades(road)
                        .stream()
                        .filter(Blockade::isApexesDefined)
                        .collect(Collectors.toSet());
                Blockade clearBlockade = null;
                double minDistance = Double.MAX_VALUE;
                double agentX = agent.getX();
                double agentY = agent.getY();
                int clearX = 0;
                int clearY = 0;
                for (Blockade blockade : blockades) {
                    int[] apexes = blockade.getApexes();
                    for (int i = 0; i < (apexes.length - 2); i += 2) {
                        double distance = NRLTacticsUtilities.getDistanceDouble(agentX, agentY, apexes[i], apexes[i + 1]);
                        if (distance < minDistance) {
                            clearBlockade = blockade;
                            minDistance = distance;
                            clearX = apexes[i];
                            clearY = apexes[i + 1];
                        }
                    }

                }
                if (clearBlockade != null) {
                    if (minDistance < this.clearDistance) {
                        Vector2D vector = new Vector2D(clearX - agentX, clearY - agentY).normalised().scale(this.clearDistance + CLEAR_DISTANCE_LIMITER);
                        clearX = (int) (agentX + vector.getX());
                        clearY = (int) (agentY + vector.getY());
                        return new ActionClear(clearX, clearY, clearBlockade);
                    }
                    return new ActionMove(Arrays.asList(agent.getPosition()), clearX, clearY);
                }
            }
        }

        return null;
    }

    private Action calculateMoveToNearestStuckAgent(WorldInfo worldInfo, PoliceForce agent, EntityID position) {
        PathWithEndPosition path = NRLTacticsUtilities.findShortestPathToHumanIDInCollectionWithEndPoint(worldInfo, pathPlanning, position, state.stuckAgentsWithPositions.keySet());
        return calculateClearPath(worldInfo, agent, path);
    }

    private Action calculateMoveToNearestStuckCivilian(WorldInfo worldInfo, PoliceForce agent, EntityID position) {
        PathWithEndPosition path = NRLTacticsUtilities.findShortestPathToHumanIDInCollectionWithEndPoint(worldInfo, pathPlanning, position, state.stuckCiviliansWithPositions.keySet());
        return calculateClearPath(worldInfo, agent, path);
    }

    private boolean hasSurroundingBuildingAndRoadContainsBlockade(WorldInfo worldInfo, EntityID position) {
        Area area = (Area) worldInfo.getEntity(position);
        return hasSurroundingBuilding(worldInfo, area)
                && area.isBlockadesDefined()
                && area.getBlockades().size() > 0;
    }

    private boolean hasSurroundingBuilding(WorldInfo worldInfo, Area area) {
        for (EntityID neighbor : area.getNeighbours()) {
            if (worldInfo.getEntity(neighbor).getStandardURN() == StandardEntityURN.BUILDING) {
                return true;
            }
        }
        return false;
    }

    private PathWithEndPosition pathToNearbyBlockedBuilding(WorldInfo worldInfo, DevelopData developData, EntityID position) {
        Collection<EntityID> nearbyIDs = worldInfo.getObjectIDsInRange(position,
                developData.getInteger("sample.tactics.MessageTool.estimatedMoveDistance", 40000));
        for (EntityID nearbyID : nearbyIDs) {
            if (worldInfo.getEntity(nearbyID).getStandardURN() == StandardEntityURN.BUILDING) {
                pathPlanning.setFrom(position);
                pathPlanning.setDestination(nearbyID);
                List<EntityID> path = pathPlanning.calc().getResult();
                if (isPathBlocked(worldInfo, path)) {
                    Pair<Integer, Integer> endPoint = worldInfo.getLocation(nearbyID);
                    return new PathWithEndPosition(path, endPoint.first(), endPoint.second());
                }
            }
        }
        return null;
    }

    private boolean isPathBlocked(WorldInfo worldInfo, List<EntityID> path) {
        if (isAreaBlocked(worldInfo, path.get(0))) {
            return true;
        }
        for (int i = 1; i < path.size() - 1; i++) {
            if (isAreaBlocked(worldInfo, path.get(i))) {
                return true;
            }
        }
        if (isAreaBlocked(worldInfo, path.get(path.size() - 1))) {
            return true;
        }
        return false;
    }


    //Can be improved upon
    private boolean isAreaBlocked(WorldInfo worldInfo, EntityID checkedArea) {
        Area area = (Area) worldInfo.getEntity(checkedArea);
        if (area.isBlockadesDefined()
                && area.getBlockades().size() > 0) {
            return true;
        }
        return false;
    }

    private Action calculateClearPath(WorldInfo worldInfo, PoliceForce agent, PathWithEndPosition path) {

        Collection<Blockade> blockadesInSurroundingPositions = NRLTacticsUtilities.getAllBlockadesFromPositionAndSurroundingPositions(worldInfo,
                (Area) agent.getPosition(worldInfo.getRawWorld()));
        for (Blockade blockade : blockadesInSurroundingPositions) {
            if (blockade.isRepairCostDefined()) {

                Point2D blockadeClearPoint = getBlockadeClearPointInRange(agent, blockade);
                if (blockadeClearPoint != null) {
                    Vector2D vector = new Vector2D(blockadeClearPoint.getX() - agent.getX(),
                            blockadeClearPoint.getY() - agent.getY()).normalised().scale(this.clearDistance + CLEAR_DISTANCE_LIMITER);
                    int clearBoxX = (int) (agent.getX() + vector.getX());
                    int clearBoxY = (int) (agent.getY() + vector.getY());
                    return new ActionClear(clearBoxX, clearBoxY, blockade);
                }
            }
        }
        return new ActionMove(path.getPath(), path.getEndX(), path.getEndY());
    }

    private Point2D getBlockadeClearPointInRange(PoliceForce agent, Blockade blockade) {
        int[] apexes = blockade.getApexes();
        double minDistance = this.clearDistance;
        Point2D clearPoint = null;
        for (int i = 0; i < apexes.length - 2; i += 2) {
            double distance = NRLTacticsUtilities.getDistanceDouble((double) agent.getX(), (double) agent.getY(), apexes[i], apexes[i + 1]);
            if (distance < minDistance) {
                clearPoint = new Point2D(apexes[i], apexes[i + 1]);
                minDistance = distance;
            }
        }
        return clearPoint;
    }

    private Action calculateClearLeftoverBlockades(WorldInfo worldInfo, EntityID agentID, EntityID position, Collection<EntityID> blockades) {
        PoliceForce agent = (PoliceForce) worldInfo.getEntity(agentID);
        for (EntityID blockadeID : blockades) {
            Blockade blockade = (Blockade) worldInfo.getEntity(blockadeID);
            if (blockade.isRepairCostDefined() && blockade.getRepairCost() > REPAIR_COST_WORTH_CLEARING) {

                Point2D blockadeClearPoint = getBlockadeClearPointInRange(agent, blockade);
                if (blockadeClearPoint != null) {
                    Vector2D vector = new Vector2D(blockadeClearPoint.getX() - agent.getX(),
                            blockadeClearPoint.getY() - agent.getY()).normalised().scale(this.clearDistance + CLEAR_DISTANCE_LIMITER);
                    int clearBoxX = (int) (agent.getX() + vector.getX());
                    int clearBoxY = (int) (agent.getY() + vector.getY());
                    return new ActionClear(clearBoxX, clearBoxY, blockade);
                }
            }
        }
        for (EntityID blockadeID : blockades) {
            Blockade blockade = (Blockade) worldInfo.getEntity(blockadeID);
            pathPlanning.setFrom(position);
            pathPlanning.setDestination(blockade.getPosition());
            return new ActionMove(pathPlanning.calc().getResult());
        }
        return new ActionRest();
    }

    private boolean unexploredRoadsRemain() {
        unexploredRoads.clear();
        unexploredRoads.removeAll(state.exploredRoads);
        return unexploredRoads.size() > 0;
    }

    private Action calculateExploreRoads(EntityID position) {
        List<EntityID> path = NRLTacticsUtilities.findShortestPathToAreaInCollection(pathPlanning, position, unexploredRoads);
        return new ActionMove(path);
    }

    private boolean unexploredBuildingsRemain(WorldInfo worldInfo) {
        unexploredBuildings.clear();
        unexploredBuildings.removeAll(state.exploredBuildings);
        unexploredBuildings.removeAll(worldInfo.getFireBuildingIDs());
        return unexploredBuildings.size() > 0;
    }

    private Action calculateExploreBuildings(WorldInfo worldInfo, EntityID startingPosition) {
        List<EntityID> shortestPath = NRLTacticsUtilities.findShortestPathToAreaInCollection(pathPlanning, startingPosition, unexploredBuildings);
        return new ActionMove(shortestPath);
    }

    private EntityID getTargetFromAction(WorldInfo worldInfo, Action action, EntityID position) {
        if (action.getClass() == ActionMove.class) {
            List<EntityID> path = ((ActionMove) action).getPath();
            if (path.size() > 0) {
                return path.get(path.size() - 1);
            }
        } else if (action.getClass() == ActionClear.class) {
            ActionClear clearAction = (ActionClear) action;
            if (clearAction.getTarget() != null) {
                return clearAction.getTarget();
            } else {
                for (StandardEntity entity : worldInfo.getObjectsInRange(clearAction.getPosX(), clearAction.getPosY(), this.clearDistance)) {
                    if (entity.getStandardURN() == StandardEntityURN.BLOCKADE) {
                        return entity.getID();
                    }
                }
            }
        } else if (action.getClass() == ActionRest.class) {
            return position;
        }
        return null;
    }

    private void sendActionMessage(MessageManager messageManager, PoliceForce agent, Action action, EntityID target) {
        int actionIndex = NRLMessageUtilities.translateActionToActionIndex(action.getClass());
        if (actionIndex != -1) {
            messageManager.addMessage(new MessagePoliceForce(true, agent, actionIndex, target));
        }
    }

}
