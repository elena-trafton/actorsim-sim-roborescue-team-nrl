package nrl.actorsim.roborescue;

import adf.agent.communication.MessageManager;
import adf.agent.develop.DevelopData;
import adf.agent.info.AgentInfo;
import adf.agent.info.ScenarioInfo;
import adf.agent.info.WorldInfo;
import adf.agent.module.ModuleManager;
import adf.agent.precompute.PrecomputeData;
import adf.component.centralized.CommandPicker;
import adf.component.communication.CommunicationMessage;
import adf.component.module.complex.TargetAllocator;
import adf.component.tactics.TacticsAmbulanceCentre;
import adf.debug.WorldViewLauncher;
import adf.sample.tactics.utils.MessageTool;
import rescuecore2.worldmodel.EntityID;

import java.util.Collection;
import java.util.Map;

public class TacticsShopAmbulanceCentre extends TacticsAmbulanceCentre {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TacticsShopAmbulanceCentre.class);

    private TargetAllocator allocator;
    private CommandPicker picker;
    private Boolean isVisualDebug;

    int agentID = 0;
    String logName = "";
    private MessageTool messageTool;

    @Override
    public void initialize(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                           ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {
        this.messageTool = new MessageTool(scenarioInfo, developData);

        switch (scenarioInfo.getMode()) {
            case PRECOMPUTATION_PHASE:
            case PRECOMPUTED:
                this.allocator = moduleManager.getModule(
                        "TacticsAmbulanceCentre.TargetAllocator",
                        "adf.sample.module.complex.SampleAmbulanceTargetAllocator");
                this.picker = moduleManager.getCommandPicker(
                        "TacticsAmbulanceCentre.CommandPicker",
                        "adf.sample.centralized.CommandPickerAmbulance");
                break;
            case NON_PRECOMPUTE:
                this.allocator = moduleManager.getModule(
                        "TacticsAmbulanceCentre.TargetAllocator",
                        "adf.sample.module.complex.SampleAmbulanceTargetAllocator");
                this.picker = moduleManager.getCommandPicker(
                        "TacticsAmbulanceCentre.CommandPicker",
                        "adf.sample.centralized.CommandPickerAmbulance");
        }
        registerModule(this.allocator);
        registerModule(this.picker);

        this.isVisualDebug = (scenarioInfo.isDebugMode()
                && moduleManager.getModuleConfig().getBooleanValue("VisualDebug", false));

        agentID = agentInfo.getID().getValue();
        String agentType = agentInfo.me().getStandardURN().name();
        logName = agentType + "-" + agentID;

		logger.info("{}: Initilialzed", logName);

    }

    @Override
    public void think(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                      ModuleManager moduleManager, MessageManager messageManager, DevelopData debugData) {
        this.messageTool.reflectMessage(agentInfo, worldInfo, scenarioInfo, messageManager);
        this.messageTool.sendRequestMessages(agentInfo, worldInfo, scenarioInfo, messageManager);
        this.messageTool.sendInformationMessages(agentInfo, worldInfo, scenarioInfo, messageManager);
        modulesUpdateInfo(messageManager);

        if (isVisualDebug) {
            WorldViewLauncher.getInstance().showTimeStep(agentInfo, worldInfo, scenarioInfo);
        }

        allocator.calc();
        Map<EntityID, EntityID> allocatorResult = allocator.getResult();
        picker.setAllocatorResult(allocatorResult);
        picker.calc();
        Collection<CommunicationMessage> messages = picker.getResult();
        for (CommunicationMessage message : messages) {
            logger.info("{} double thing", message);
            messageManager.addMessage(message);
        }
    }

    @Override
    public void resume(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                       ModuleManager moduleManager, PrecomputeData precomputeData, DevelopData debugData) {
        modulesResume(precomputeData);

        if (isVisualDebug) {
            WorldViewLauncher.getInstance().showTimeStep(agentInfo, worldInfo, scenarioInfo);
        }
    }

    @Override
    public void preparate(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                          ModuleManager moduleManager, DevelopData debugData) {
        modulesPreparate();

        if (isVisualDebug) {
            WorldViewLauncher.getInstance().showTimeStep(agentInfo, worldInfo, scenarioInfo);
        }
    }


}
