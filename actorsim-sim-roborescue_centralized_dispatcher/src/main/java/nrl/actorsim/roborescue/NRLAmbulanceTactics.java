package nrl.actorsim.roborescue;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import adf.agent.action.Action;
import adf.agent.action.ambulance.ActionLoad;
import adf.agent.action.ambulance.ActionRescue;
import adf.agent.action.ambulance.ActionUnload;
import adf.agent.action.common.ActionMove;
import adf.agent.action.common.ActionRest;
import adf.agent.communication.MessageManager;
import adf.agent.communication.standard.bundle.centralized.CommandAmbulance;
import adf.agent.communication.standard.bundle.centralized.CommandScout;
import adf.agent.communication.standard.bundle.information.MessageAmbulanceTeam;
import adf.agent.develop.DevelopData;
import adf.agent.info.AgentInfo;
import adf.agent.info.ScenarioInfo;
import adf.agent.info.WorldInfo;
import adf.agent.module.ModuleManager;
import adf.agent.precompute.PrecomputeData;
import adf.component.centralized.CommandExecutor;
import adf.component.communication.CommunicationMessage;
import adf.component.module.algorithm.PathPlanning;
import adf.component.tactics.TacticsAmbulanceTeam;
import adf.sample.centralized.CommandExecutorAmbulance;
import adf.sample.centralized.CommandExecutorScout;
import adf.sample.tactics.utils.MessageTool;
import rescuecore2.misc.Pair;
import rescuecore2.standard.entities.AmbulanceTeam;
import rescuecore2.standard.entities.Building;
import rescuecore2.standard.entities.Civilian;
import rescuecore2.standard.entities.Human;
import rescuecore2.standard.entities.StandardEntity;
import rescuecore2.standard.entities.StandardEntityURN;
import rescuecore2.worldmodel.EntityID;
import test_team.module.algorithm.AStarPathPlanning;

import static rescuecore2.standard.entities.StandardEntityURN.BUILDING;
import static rescuecore2.standard.entities.StandardEntityURN.CIVILIAN;
import static rescuecore2.standard.entities.StandardEntityURN.REFUGE;

public class NRLAmbulanceTactics extends TacticsAmbulanceTeam implements TacticsCommandable {
    final static private org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(NRLAmbulanceTactics.class);

    EntityID agentID;
    String shortName;

    private Boolean isVisualDebug;
    private CommunicationMessage recentCommand;

    private MessageTool messageTool;
    private PathPlanning pathPlanning;

    NRLAgentStateOptions stateOptions = null;
    NRLAgentState state = null;
    private HashSet<EntityID> unexploredBuildings = new HashSet<>();

    private Pair<Integer, Integer> lastLocation;
    private Action lastAction;

    private CommandExecutor<CommandScout> commandExecutorScout;
    private CommandExecutor<CommandAmbulance> commandExecutorAmbulance;
    private boolean autonomousActionEnabled = false;

    @Override
    public String getShortName() {
        return shortName;
    }

    @Override
    public void initialize(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                           ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {
        worldInfo.indexClass(
                CIVILIAN,
                StandardEntityURN.FIRE_BRIGADE,
                StandardEntityURN.POLICE_FORCE,
                StandardEntityURN.AMBULANCE_TEAM,
                StandardEntityURN.ROAD,
                StandardEntityURN.HYDRANT,
                BUILDING,
                REFUGE,
                StandardEntityURN.GAS_STATION,
                StandardEntityURN.AMBULANCE_CENTRE,
                StandardEntityURN.FIRE_STATION,
                StandardEntityURN.POLICE_OFFICE
        );
        this.isVisualDebug = (scenarioInfo.isDebugMode()
                && moduleManager.getModuleConfig().getBooleanValue("VisualDebug", false));
        this.recentCommand = null;
        this.messageTool = new MessageTool(scenarioInfo, developData);
        this.pathPlanning = new AStarPathPlanning(agentInfo, worldInfo, scenarioInfo, moduleManager, developData);
        this.commandExecutorAmbulance = new CommandExecutorAmbulance(agentInfo, worldInfo, scenarioInfo, moduleManager, developData);
        this.commandExecutorScout = new CommandExecutorScout(agentInfo, worldInfo, scenarioInfo, moduleManager, developData);
        this.lastLocation = null;
        this.lastAction = null;

        registerModule(pathPlanning);
        registerModule(commandExecutorAmbulance);
        registerModule(commandExecutorScout);

        agentID = agentInfo.getID();
        String agentType = agentInfo.me().getStandardURN().name();
        shortName = agentType + "-" + agentID;

        stateOptions = new NRLAgentStateOptions()
                .enableUpdateUsingCommunications();
        state = new NRLAgentState(this, stateOptions);
        state.initialize(worldInfo);

        logger.info("{}: Initilialzed", shortName);
    }

    @Override
    public void precompute(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                           ModuleManager moduleManager, PrecomputeData precomputeData, DevelopData developData) {
        modulesPrecompute(precomputeData);
    }

    @Override
    public void resume(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                       ModuleManager moduleManager, PrecomputeData precomputeData, DevelopData developData) {

        modulesResume(precomputeData);

        NRLTacticsUtilities.showTimestep(isVisualDebug, agentInfo, worldInfo, scenarioInfo);
    }

    @Override
    public void preparate(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                          ModuleManager moduleManager, DevelopData developData) {

        modulesPreparate();

        NRLTacticsUtilities.showTimestep(isVisualDebug, agentInfo, worldInfo, scenarioInfo);
    }

    @Override
    public Action think(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                        ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {
        this.messageTool.reflectMessage(agentInfo, worldInfo, scenarioInfo, messageManager);
        this.messageTool.sendRequestMessages(agentInfo, worldInfo, scenarioInfo, messageManager);
        this.messageTool.sendInformationMessages(agentInfo, worldInfo, scenarioInfo, messageManager);

        modulesUpdateInfo(messageManager);
        updateInfoFromSelf(agentInfo, worldInfo);
        state.update(worldInfo, messageManager);
        NRLTacticsUtilities.showTimestep(isVisualDebug, agentInfo, worldInfo, scenarioInfo);

        AmbulanceTeam agent = (AmbulanceTeam) agentInfo.me();
        Action action = null;

        setRecentCommand(agentInfo, messageManager);
        if (CentralizedDispatcher.isInitialized()) {
            recentCommand = CentralizedDispatcher.getInstance().getCommandMessage(this.agentID);
            logger.debug("{}: dispatcher command {}", shortName, NRLMessageUtilities.toString(recentCommand));
        }
        if (this.recentCommand != null) {
            action = followCommand(recentCommand);
            logger.debug("{}: command -> action {}", shortName, NRLMessageUtilities.toString(action));
        }

        if (autonomousActionEnabled) {
            if (action == null) {
                action = actAutonomously(agentInfo, worldInfo, agent);
            }
        }

        if ((action == null)
            || (action.getClass() == ActionMove.class
                && ((ActionMove) action).getPath() == null)) {
            action = new ActionRest();
            logger.debug("{}: no action provided, so sending action {}", shortName, NRLMessageUtilities.toString(action));
        }

        EntityID target = getTargetFromAction(action, agentInfo.getPosition());


        this.lastAction = action;
        this.lastLocation = agentInfo.me().getLocation(worldInfo.getRawWorld());
        logger.debug("{}: sending action {}", shortName, NRLMessageUtilities.toString(action));
        sendActionMessage(messageManager, agent, action, target);
        return action;
    }


    private void updateInfoFromSelf(AgentInfo agentInfo, WorldInfo worldInfo) {
        if (agentInfo.getPositionArea().getStandardURN() == BUILDING) {
            state.exploredBuildings.add(agentInfo.getPosition());
        }
        Human me = (Human) agentInfo.me();
        Pair<Integer, Integer> myLocation = me.getLocation(worldInfo.getRawWorld());
        if (agentInfo.getTime() > NRLTacticsUtilities.TIMESTEPS_UNTIL_AGENTS_CAN_MOVE
                && this.lastAction != null
                && this.lastAction.getClass() == ActionMove.class
                && NRLTacticsUtilities.hadEffectivelyNotMoved(lastLocation, myLocation)
                && !NRLTacticsUtilities.isHumanStuck(worldInfo, me)) {
            EntityID target = getTargetFromAction(this.lastAction, null);
            StandardEntityURN targetType = worldInfo.getEntity(target).getStandardURN();
            if (targetType == BUILDING) {
                state.exploredBuildings.add(target);
            }
        }
    }

    private void setRecentCommand(AgentInfo agentInfo, MessageManager messageManager) {
        List<CommunicationMessage> messages = messageManager.getReceivedMessageList();
        for (CommunicationMessage message : messages) {
            if (message instanceof CommandScout) {
                CommandScout command = (CommandScout) message;
                if (command.isToIDDefined()) {
                    EntityID toID = command.getToID();
                    if (toID != null
                            && toID.equals(agentInfo.getID())) {
                        recentCommand = command;
                    }
                }
            }
            if (message instanceof  CommandAmbulance) {
                CommandAmbulance command = (CommandAmbulance) message;
                if (command.isToIDDefined()) {
                    EntityID toID = command.getToID();
                    if (toID != null
                            && toID.equals(agentInfo.getID())) {
                        recentCommand = command;
                    }
                }
            }
        }
    }

    private Action followCommand(CommunicationMessage command) {
        if (command instanceof CommandScout) {
            commandExecutorScout.setCommand((CommandScout) command);
            return commandExecutorScout.calc().getAction();
        }
        if (command instanceof CommandAmbulance) {
            commandExecutorAmbulance.setCommand((CommandAmbulance) command);
            return commandExecutorAmbulance.calc().getAction();
        }
        return null;
    }

    private Action actAutonomously(AgentInfo agentInfo, WorldInfo worldInfo, AmbulanceTeam agent) {
        Action action = null;
        StandardEntity area = worldInfo.getEntity(agentInfo.getPosition());
        if (agentInfo.someoneOnBoard() != null) {
            action = calculateTransportAction(agentInfo, worldInfo);
        } else if (area.getStandardURN() == BUILDING) {
            action = calculateRescueAction(worldInfo, (Building) area);
        }
        if (action == null) {
            if (!state.buildingsWithBuriedPeople.isEmpty()) {
                action = calculateMoveToNextBuriedPerson(agentInfo.getPosition());
            } else if (!state.injuredCiviliansNotInRefugeWithPosition.isEmpty()) {
                action = calculateMoveToNextInjuredCivilian(agentInfo.getPosition());
            } else if (unexploredBuildingsRemain(worldInfo)) {
                action = calculateExploreBuildings(worldInfo, agentInfo.getPosition());
            } else {
                action = new ActionRest();
            }
        }
        return action;

    }

    private Action calculateTransportAction(AgentInfo agentInfo, WorldInfo worldInfo) {
        StandardEntityURN currentPositionType = agentInfo.getPositionArea().getStandardURN();
        if (currentPositionType == REFUGE || agentInfo.someoneOnBoard().getHP() == 0) {
            return new ActionUnload();
        }
        List<EntityID> shortestPath = NRLTacticsUtilities.findShortestPathToAreaInCollection(this.pathPlanning,
                agentInfo.getPosition(), worldInfo.getEntityIDsOfType(REFUGE));
        return new ActionMove(shortestPath);
    }

    private Action calculateRescueAction(WorldInfo worldInfo, Building building) {
        Collection<Human> buriedHumans = worldInfo.getBuriedHumans(building);
        if (!buriedHumans.isEmpty()) {
            for (Human human : buriedHumans) {
                if (human.isHPDefined()
                        && human.getHP() > 0) {
                    return new ActionRescue(human);
                }
            }
        }
        state.buildingsWithBuriedPeople.remove(building.getID());
        Collection<StandardEntity> civilians = worldInfo.getEntitiesOfType(CIVILIAN);
        if (!civilians.isEmpty()) {
            for (StandardEntity person : civilians) {
                Civilian civilian = (Civilian) person;

                if (civilian.getPosition().equals(building.getID())
                        && civilian.getHP() > 0
                        && civilian.getDamage() != 0) {
                    state.injuredCiviliansNotInRefugeWithPosition.remove(civilian.getID());
                    return new ActionLoad(civilian);
                }
            }
        }

        return null;
    }

    private Action calculateMoveToNextBuriedPerson(EntityID startingPosition) {
        List<EntityID> shortestPath = NRLTacticsUtilities.findShortestPathToAreaInCollection(this.pathPlanning,
                startingPosition, state.buildingsWithBuriedPeople);
        return new ActionMove(shortestPath);
    }

    private Action calculateMoveToNextInjuredCivilian(EntityID startingPosition) {
        List<EntityID> shortestPath = NRLTacticsUtilities.findShortestPathToAreaInCollection(pathPlanning, startingPosition, state.injuredCiviliansNotInRefugeWithPosition.values());
        return new ActionMove(shortestPath);
    }

    private boolean unexploredBuildingsRemain(WorldInfo worldInfo) {
        unexploredBuildings.clear();
        unexploredBuildings.addAll(state.buildings);
        unexploredBuildings.removeAll(state.exploredBuildings);
        unexploredBuildings.removeAll(worldInfo.getFireBuildingIDs());
        return unexploredBuildings.size() > 0;
    }

    private Action calculateExploreBuildings(WorldInfo worldInfo, EntityID startingPosition) {
        List<EntityID> shortestPath = NRLTacticsUtilities.findShortestPathToAreaInCollection(pathPlanning, startingPosition, unexploredBuildings);
        return new ActionMove(shortestPath);
    }

    private EntityID getTargetFromAction(Action action, EntityID position) {
        if (action.getClass() == ActionMove.class) {
            List<EntityID> path = ((ActionMove) action).getPath();
            if (path.size() > 0) {
                return path.get(path.size() - 1);
            }
        } else if (action.getClass() == ActionRescue.class) {
            return ((ActionRescue) action).getTarget();
        } else if (action.getClass() == ActionLoad.class) {
            return ((ActionLoad) action).getTarget();
        } else if (action.getClass() == ActionUnload.class || action.getClass() == ActionRest.class) {
            return position;
        }
        return null;
    }

    private void sendActionMessage(MessageManager messageManager, AmbulanceTeam agent,
                                   Action action, EntityID target) {
        int actionIndex = NRLMessageUtilities.translateActionToActionIndex(action.getClass());
        if (actionIndex != -1) {
            messageManager.addMessage(new MessageAmbulanceTeam(true, agent, actionIndex, target));
        }
    }

}
