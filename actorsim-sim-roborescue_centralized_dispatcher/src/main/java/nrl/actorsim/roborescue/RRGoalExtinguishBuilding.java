package nrl.actorsim.roborescue;

import adf.agent.communication.standard.bundle.centralized.CommandFire;
import nrl.actorsim.domain.StateVariable;
import nrl.actorsim.domain.WorldObject;
import nrl.actorsim.domain.WorldType;

import java.util.Collections;

public class RRGoalExtinguishBuilding extends RRGoalBase {
    RRGoalExtinguishBuilding() {
        super(CommandFire.ACTION_EXTINGUISH,
                RRPlanningDomain.ON_FIRE,
                CompletionCondition.MISSING_FROM_MEMORY,
                Collections.singletonList(RRPlanningDomain.BRIGADE.getUrn()));
    }

    private RRGoalExtinguishBuilding(RRGoalExtinguishBuilding template, RRWorldObject building) {
        super(template, building);
    }

    @Override
    public RRGoalBase instance(WorldObject obj) {
        if (obj instanceof RRWorldObject) {
            if (isBuilding(obj)) {
                return new RRGoalExtinguishBuilding(this, (RRWorldObject) obj);
            }
        }
        return NULL_RR_BASE_GOAL;
    }

    @Override
    public boolean matchesVariable(StateVariable sv) {
        return super.matchesVariable(sv)
                && sv.hasBindings()
                && isBuilding(sv.getBinding(0));
    }

    private boolean isBuilding(WorldType other) {
        return other.equalsOrInheritsFromType(RRPlanningDomain.BUILDING);
    }
}
