package nrl.actorsim.roborescue;

import static rescuecore2.standard.entities.StandardEntityURN.HYDRANT;
import static rescuecore2.standard.entities.StandardEntityURN.REFUGE;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import adf.agent.action.Action;
import adf.agent.action.common.ActionMove;
import adf.agent.action.common.ActionRest;
import adf.agent.action.fire.ActionExtinguish;
import adf.agent.action.fire.ActionRefill;
import adf.agent.communication.MessageManager;
import adf.agent.communication.standard.bundle.centralized.CommandFire;
import adf.agent.communication.standard.bundle.centralized.CommandScout;
import adf.agent.communication.standard.bundle.information.MessageFireBrigade;
import adf.agent.develop.DevelopData;
import adf.agent.info.AgentInfo;
import adf.agent.info.ScenarioInfo;
import adf.agent.info.WorldInfo;
import adf.agent.module.ModuleManager;
import adf.agent.precompute.PrecomputeData;
import adf.component.centralized.CommandExecutor;
import adf.component.communication.CommunicationMessage;
import adf.component.module.algorithm.PathPlanning;
import adf.component.tactics.TacticsFireBrigade;
import adf.sample.centralized.CommandExecutorFire;
import adf.sample.centralized.CommandExecutorScout;
import adf.sample.tactics.utils.MessageTool;
import rescuecore2.standard.entities.Building;
import rescuecore2.standard.entities.FireBrigade;
import rescuecore2.standard.entities.StandardEntity;
import rescuecore2.standard.entities.StandardEntityURN;
import rescuecore2.worldmodel.EntityID;
import test_team.module.algorithm.AStarPathPlanning;

public class NRLFireBrigadeTactics extends TacticsFireBrigade implements TacticsCommandable {
    final static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(NRLAmbulanceTactics.class);

    EntityID agentID;
    String shortName;

    private PathPlanning pathPlanning;

    private MessageTool messageTool;
    private boolean isVisualDebug;
    private CommunicationMessage recentCommand;

    private CommandExecutor<CommandScout> commandExecutorScout;
    private CommandExecutor<CommandFire> commandExecutorFireBrigade;

    NRLAgentStateOptions stateOptions = null;
    NRLAgentState state = null;
    HashSet<EntityID> unexploredRoads = new HashSet<>();

    private boolean refillFlag;
    private int maxWaterCapacity;
    private int maxExtinguishPower;
    private int extinguishRange;
    private boolean autonomousActionEnabled = false;

    @Override
    public String getShortName() {
        return null;
    }

    @Override
    public void initialize(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                           ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {
        worldInfo.indexClass(
                StandardEntityURN.ROAD,
                StandardEntityURN.HYDRANT,
                StandardEntityURN.BUILDING,
                StandardEntityURN.REFUGE,
                StandardEntityURN.GAS_STATION,
                StandardEntityURN.AMBULANCE_CENTRE,
                StandardEntityURN.FIRE_STATION,
                StandardEntityURN.POLICE_OFFICE
        );

        this.messageTool = new MessageTool(scenarioInfo, developData);

        this.isVisualDebug = (scenarioInfo.isDebugMode()
                && moduleManager.getModuleConfig().getBooleanValue("VisualDebug", false));

        this.pathPlanning = new AStarPathPlanning(agentInfo, worldInfo, scenarioInfo, moduleManager, developData);
        this.recentCommand = null;
        this.refillFlag = false;
        this.maxWaterCapacity = scenarioInfo.getFireTankMaximum();
        this.maxExtinguishPower = scenarioInfo.getFireExtinguishMaxSum();
        this.extinguishRange = scenarioInfo.getFireExtinguishMaxDistance();
        this.commandExecutorFireBrigade = new CommandExecutorFire(agentInfo, worldInfo, scenarioInfo, moduleManager, developData);
        this.commandExecutorScout = new CommandExecutorScout(agentInfo, worldInfo, scenarioInfo, moduleManager, developData);
        registerModule(this.commandExecutorFireBrigade);
        registerModule(this.commandExecutorScout);
        registerModule(this.pathPlanning);

        stateOptions = new NRLAgentStateOptions();
        stateOptions.updateUsingCommunications = true;
        stateOptions.parseCivilianMessages = true;
        stateOptions.parseFireBrigadeMessages = true;
        stateOptions.parseRoadsAndBuildings = true;
        state = new NRLAgentState(this, stateOptions);
        state.initialize(worldInfo);

        agentID = agentInfo.getID();
        String agentType = agentInfo.me().getStandardURN().name();
        shortName = agentType + "-" + agentID;
        logger.info("{}: Initilialzed", shortName);

    }

    @Override
    public void precompute(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                           ModuleManager moduleManager, PrecomputeData precomputeData, DevelopData developData) {

        modulesPrecompute(precomputeData);
    }

    @Override
    public void resume(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo, ModuleManager moduleManager,
                       PrecomputeData precomputeData, DevelopData developData) {
        modulesResume(precomputeData);

        NRLTacticsUtilities.showTimestep(isVisualDebug, agentInfo, worldInfo, scenarioInfo);

    }

    @Override
    public void preparate(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                          ModuleManager moduleManager, DevelopData developData) {
        modulesPreparate();

        NRLTacticsUtilities.showTimestep(isVisualDebug, agentInfo, worldInfo, scenarioInfo);
    }

    @Override
    public Action think(AgentInfo agentInfo, WorldInfo worldInfo, ScenarioInfo scenarioInfo,
                        ModuleManager moduleManager, MessageManager messageManager, DevelopData developData) {
        this.messageTool.reflectMessage(agentInfo, worldInfo, scenarioInfo, messageManager);
        this.messageTool.sendRequestMessages(agentInfo, worldInfo, scenarioInfo, messageManager);
        this.messageTool.sendInformationMessages(agentInfo, worldInfo, scenarioInfo, messageManager);
        modulesUpdateInfo(messageManager);
        updateInfoFromSelf(agentInfo, worldInfo);
        state.update(worldInfo, messageManager);
        NRLTacticsUtilities.showTimestep(isVisualDebug, agentInfo, worldInfo, scenarioInfo);

        FireBrigade agent = (FireBrigade) agentInfo.me();
        Action action = null;

        setRecentCommand(agentInfo, messageManager);
        recentCommand = CentralizedDispatcher.getInstance().getCommandMessage(this.agentID);
        if (this.recentCommand != null) {
            action = followCommand(recentCommand);
        }

        if (autonomousActionEnabled) {
            if (action == null) {
                action = actAutonomously(agentInfo, worldInfo, agent);
            }
        }

        if ((action == null)
            || ((action.getClass() == ActionMove.class)
                && ((ActionMove) action).getPath() == null)) {
            action = new ActionRest();
        }

        EntityID target = getTargetFromAction(action, agentInfo.getPosition());


        logger.debug("{}: sending action {}", shortName, action);
        sendActionMessage(messageManager, agent, action, target);

        return action;
    }

    private void updateInfoFromSelf(AgentInfo agentInfo, WorldInfo worldInfo) {
        if (agentInfo.getPositionArea().getStandardURN() == StandardEntityURN.ROAD) {
            state.exploredRoads.add(agentInfo.getPosition());
        }
    }

    private void setRecentCommand(AgentInfo agentInfo, MessageManager messageManager) {
        for (CommunicationMessage message : messageManager.getReceivedMessageList()) {
            if (message instanceof CommandScout) {
                CommandScout command = (CommandScout) message;
                if (command.isToIDDefined() && Objects.requireNonNull(command.getToID()).getValue() == agentInfo.getID().getValue()) {
                    recentCommand = command;
                }
            }
            if (message instanceof CommandFire) {
                CommandFire command = (CommandFire) message;
                if (command.isToIDDefined() && Objects.requireNonNull(command.getToID()).getValue() == agentInfo.getID().getValue()) {
                    recentCommand = command;
                }
            }
        }
    }

    private Action followCommand(CommunicationMessage command) {
        if (command.getClass() == CommandScout.class) {
            commandExecutorScout.setCommand((CommandScout) command);
            return commandExecutorScout.calc().getAction();
        }
        if (command.getClass() == CommandFire.class) {
            commandExecutorFireBrigade.setCommand((CommandFire) command);
            return commandExecutorFireBrigade.calc().getAction();
        }
        return null;
    }

    private Action actAutonomously(AgentInfo agentInfo, WorldInfo worldInfo, FireBrigade agent) {
        Action action = null;
        EntityID position = agent.getPosition();
        this.refillFlag = needsRefill(worldInfo, agent);
        if (this.refillFlag) {
            action = calcActionRefill(agentInfo, worldInfo, position);
        } else if (!worldInfo.getFireBuildings().isEmpty()) {
            action = calcExtinguishAction(agentInfo, worldInfo);
        } else if (unexploredRoadsRemain()) {
            action = calcExploreNearestRoad(agentInfo, worldInfo);
        } else if (!isTankFull(agent)) {
            action = calcActionRefill(agentInfo, worldInfo, position);
        } else {
            action = new ActionRest();
        }
        return action;
    }


    private boolean unexploredRoadsRemain() {
        unexploredRoads.clear();
        unexploredRoads.removeAll(state.exploredRoads);
        return unexploredRoads.size() > 0;
    }

    private boolean needsRefill(WorldInfo worldInfo, FireBrigade agent) {
        if (refillFlag) {
            StandardEntityURN positionURN = Objects.requireNonNull(worldInfo.getPosition(agent)).getStandardURN();
            return !(positionURN == REFUGE || positionURN == HYDRANT) || agent.getWater() < this.maxWaterCapacity;
        }
        return agent.getWater() < this.maxExtinguishPower;
    }

    private Action calcActionRefill(AgentInfo agentInfo, WorldInfo worldInfo, EntityID position) {
        StandardEntity positionEntity = worldInfo.getEntity(position);
        if (positionEntity.getStandardURN() == StandardEntityURN.REFUGE || positionEntity.getStandardURN() == StandardEntityURN.HYDRANT) {
            return new ActionRefill();
        }
        List<EntityID> path = NRLTacticsUtilities.findShortestPathToAreaInCollection(pathPlanning,
                position, worldInfo.getEntityIDsOfType(StandardEntityURN.REFUGE, StandardEntityURN.HYDRANT));
        return new ActionMove(path);
    }

    private Action calcExtinguishAction(AgentInfo agentInfo, WorldInfo worldInfo) {
        Action action = calcExtinguishBuildingsWithPeopleInside(agentInfo, worldInfo);
        if (action == null) {
            action = calcExtinguishClosestBuildings(agentInfo, worldInfo);
        }
        return action;
    }

    private Action calcExtinguishBuildingsWithPeopleInside(AgentInfo agentInfo, WorldInfo worldInfo) {
        Set<EntityID> buildingsOnFireWithBuriedPeople = new HashSet<EntityID>(state.buildingsWithBuriedPeople);
        buildingsOnFireWithBuriedPeople.retainAll(worldInfo.getFireBuildingIDs());
        if (!buildingsOnFireWithBuriedPeople.isEmpty()) {
            for (EntityID burningBuilding : buildingsOnFireWithBuriedPeople) {
                Building building = (Building) worldInfo.getEntity(burningBuilding);
                if (worldInfo.getDistance(burningBuilding, agentInfo.getID()) < this.extinguishRange && building.isOnFire()) {
                    return new ActionExtinguish(burningBuilding, this.maxExtinguishPower);
                }
            }
            List<EntityID> path = NRLTacticsUtilities.findShortestPathToAreaInCollection(pathPlanning,
                    agentInfo.getPosition(), buildingsOnFireWithBuriedPeople);
            return new ActionMove(path);
        }
        return null;
    }

    private Action calcExtinguishClosestBuildings(AgentInfo agentInfo, WorldInfo worldInfo) {
        for (EntityID burningBuilding : worldInfo.getFireBuildingIDs()) {
            Building building = (Building) worldInfo.getEntity(burningBuilding);
            if (worldInfo.getDistance(burningBuilding, agentInfo.getID()) < this.extinguishRange && building.isOnFire()) {
                return new ActionExtinguish(burningBuilding, this.maxExtinguishPower);
            }
        }
        List<EntityID> path = NRLTacticsUtilities.findShortestPathToAreaInCollection(pathPlanning,
                agentInfo.getPosition(), worldInfo.getFireBuildingIDs());
        return new ActionMove(path);
    }

    private Action calcExploreNearestRoad(AgentInfo agentInfo, WorldInfo worldInfo) {
        List<EntityID> path = NRLTacticsUtilities.findShortestPathToAreaInCollection(pathPlanning, agentInfo.getPosition(), unexploredRoads);
        return new ActionMove(path);
    }

    private boolean isTankFull(FireBrigade agent) {
        return agent.getWater() == this.maxWaterCapacity;
    }

    private EntityID getTargetFromAction(Action action, EntityID position) {
        if (action.getClass() == ActionMove.class) {
            List<EntityID> path = ((ActionMove) action).getPath();
            if (path.size() > 0) {
                return path.get(path.size() - 1);
            }
        } else if (action.getClass() == ActionExtinguish.class) {
            return ((ActionExtinguish) action).getTarget();
        } else if (action.getClass() == ActionRefill.class || action.getClass() == ActionRest.class) {
            return position;
        }
        return null;
    }

    private void sendActionMessage(MessageManager messageManager, FireBrigade agent, Action action, EntityID target) {
        int actionIndex = NRLMessageUtilities.translateActionToActionIndex(action.getClass());
        if (actionIndex != -1) {
            messageManager.addMessage(new MessageFireBrigade(true, agent, actionIndex, target));
        }
    }

}
