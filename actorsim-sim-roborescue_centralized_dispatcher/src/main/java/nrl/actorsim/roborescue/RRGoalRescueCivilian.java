package nrl.actorsim.roborescue;

import adf.agent.communication.standard.bundle.centralized.CommandAmbulance;
import nrl.actorsim.domain.StateVariable;
import nrl.actorsim.domain.WorldObject;
import nrl.actorsim.domain.WorldType;

import java.util.Collections;

public class RRGoalRescueCivilian extends RRGoalBase {
    RRGoalRescueCivilian() {
        super(CommandAmbulance.ACTION_RESCUE,
                RRPlanningDomain.NEEDS_RESCUE,
                CompletionCondition.MISSING_FROM_MEMORY,
                Collections.singletonList(RRPlanningDomain.AMBULANCE.getUrn()));
    }

    private RRGoalRescueCivilian(RRGoalRescueCivilian template, RRHumanObject civilian) {
        super(template, civilian);
    }

    @Override
    public RRGoalBase instance(WorldObject obj) {
        if (obj instanceof RRHumanObject) {
            if (isCivilian(obj)) {
                return new RRGoalRescueCivilian(this, (RRHumanObject) obj);
            }
        }
        return NULL_RR_BASE_GOAL;
    }

    @Override
    public boolean matchesVariable(StateVariable sv) {
        return super.matchesVariable(sv)
                && sv.hasBindings()
                && isCivilian(sv.getBinding(0));
    }

    private boolean isCivilian(WorldType other) {
        return other.equalsOrInheritsFromType(RRPlanningDomain.CIVILIAN);
    }
}
